trainFerns <- function(X,Y,D, opt=FALSE){
	#wybor prorektorow
	p <- ncol(X)
	index <- 1:p
	#attributes <- as.numeric(sample(index, D)) 
	#selected <- rep(FALSE, p)
	#selected[attributes] <- TRUE
  	selected<-sample(index,D, replace=TRUE)
	X <- X[,selected]
  X<-data.frame(X)
	randomDivision <- function(X){
		# założenie: atrybut albo jest liczbą, albo faktorem
		if(is.numeric(X)){
			div<-mean(sample(X,2));
			return(div)
		}
		else{ #czyli jest faktorem
			categories <- levels(X)
			div<-categories[intToBits(sample(2^length(categories)-2,1))[1:length(categories)]>0];
			return(div)
		}
	}
	randomDivisions<-function(X) lapply(X,randomDivision)
	
	makeSimpleFern<- function(X,Y,divisions){
	# w tym przypadku X to juz okrojona ramka danych z kolumnami oznaczajacymi wybrane atrybuty
		D<-length(divisions)
		giveLeaf <- function(X,divisions){
		  D<-length(divisions)
		  decisions<-rep(0,nrow(X));
		  for(i in 1:D){
		    if(is.numeric(X[,i])){
		      decisions<- ifelse( is.na(X[,i]),sample(c(decisions*2,(decisions*2+1)),1),decisions*2+(X[,i] >= divisions[[i]])  )
		    }
		    else{
		      decisions<- ifelse(is.na(X[,i]),sample(c(decisions*2,(decisions*2+1)),1)  ,decisions*2+(X[,i] %in% divisions[[i]])  )
		    }  
		  }
		  return(decisions+1)
		}

		leaves <- factor(giveLeaf(X,	divisions),levels=1:2^D);
		licznosci <- as.matrix(table(leaves,Y)); #! Trick Dirichleta
 
 		
 		licznosci <- t(apply(licznosci, 1, function(x) x/(table(Y)/length(Y))))

 
		prawd<-licznosci/rowSums(licznosci)
		wagi<-rowSums(licznosci)/nrow(X);
		return(list(prawd=prawd,licznosci=licznosci,wagi=wagi))
	}

	optimizeDivisions <- function(X,Y,startDivisions){ #X - selected
		entropia<-function(X,Y,div){
  			fern<-makeSimpleFern(X,Y,div)
  			prawd<-fern$prawd;
  			wagi<-fern$wagi;
  			sum(wagi*rowSums(prawd*log(prawd),na.rm=TRUE))
  		}
	
		D <- length(startDivisions)
		currentDivisions <- list()
    		currentDivisions <- startDivisions
    	        	 #! Żeby plus-minus zbiegało na iris-ie
	    	for(i in 1:D){
    			if (is.numeric(X[,i])){
				S <- sort(X[,i])
				Z <- S[-1] - diff(S)/2
				sapply(Z,function(thre){
					currentDivisions[[i]] <- thre
					entropia(X,Y,currentDivisions)
				})->ent;
				bestDiv <- Z[which.max(ent)]
			}
			else{
				X[,i] <- as.factor(X[,i])
				lev<-levels(X[,i])
				k<-nlevels(X[,i])
				f<-function(x){
					currentDivisions[[i]] <- lev[intToBits(x)[1:k]>0]
					return(entropia(X,Y,currentDivisions))
	      			}
      				v<-1:(2^k-2)
      				ent<-sapply(v,f)
      				best<-v[which.max(ent)]
	      			bestDiv<-lev[intToBits(best)[1:k]>0]
			}
			#message(sprintf("Optimized level #%s, max H=%0.3f",i,max(ent)));
			currentDivisions[[i]] <- bestDiv
		}
   		return(currentDivisions)
	}

	if(opt == TRUE){
	    divisions <- optimizeDivisions(X,Y,randomDivisions(X))
	    optFern <- makeSimpleFern(X,Y,divisions)
	    tmp <- 1 + optFern$licznosci
	}
	else{
	    divisions <- randomDivisions(X)
	    randFern <- makeSimpleFern(X,Y, divisions)
	    tmp <- 1 + randFern$licznosci
	}
	denominator <- apply(tmp,1,sum)
	tmp <- tmp/denominator
	scores <- log(tmp) + log(nlevels(Y))

	scores <- matrix(scores, ncol = nlevels(Y))
	colnames(scores) <- levels(Y)
	rownames(scores) <- 1:2^D
	return(list(selected=selected, divisions = divisions, scores=scores))
}

###########################################

predictFernRaw<-function(fern,X){
  
	D<-length(fern$selected)
	X <- data.frame(X[,fern$selected])
	giveLeaf <- function(X,divisions){
	  D<-length(divisions)
	  decisions<-rep(0,nrow(X));
	  for(i in 1:D){
	    if(is.numeric(X[,i])){
	      decisions<- ifelse( is.na(X[,i]),sample(c(decisions*2,(decisions*2+1)),1),decisions*2+(X[,i] >= divisions[[i]])  )
	    }
	    else{
	      decisions<- ifelse(is.na(X[,i]),sample(c(decisions*2,(decisions*2+1)),1)  ,decisions*2+(X[,i] %in% divisions[[i]])  )
	    }  
	  }
	  return(decisions+1)
	}
	leaves <- factor(giveLeaf(X,fern$divisions),levels=1:2^D);
 	results<-fern$scores[as.numeric(leaves),] 
 	return(results)
}

###########################################

trainFernForest <- function(X,Y,N,D,opt=FALSE,importance=FALSE){

	makeFern <- function(X,Y,D){ #tworzy paproc na probce bootstrapowej z X, zwraca paproć i macierz z predykcją oob
		bag_mask <- sample(1:nrow(X),nrow(X),TRUE)
		fern <- trainFerns(X[bag_mask,],Y[bag_mask],D,opt)
	 	oob_mask<-!((1:nrow(X)) %in% bag_mask)
	 	predMatrix<-matrix(NA,nrow=nrow(X),ncol=nlevels(Y))
		predOob <- predictFernRaw(fern, X[oob_mask,])
		predMatrix[oob_mask,]<-predOob
		
		if(importance == TRUE){
		    attrImportance <- rep(NA, ncol(X))
		    names(attrImportance) <- colnames(X)
		    for(i in 1:ncol(X)){
			if( (i %in% fern$selected) & (is.na(attrImportance[i])) ){
			    X[oob_mask,i] <- sample(X[oob_mask,i], sum(oob_mask))
			    predOob2 <- predictFernRaw(fern,X[oob_mask,])
			    classNumbers <- sapply(Y[oob_mask], function(x) which(factor(levels(Y))==x ))
			    indexMatrix <- cbind(1:length(classNumbers), classNumbers)
			    differences <- apply(indexMatrix, 1, function(x)(predOob-predOob2)[x[1],x[2]])
			    attrImportance[i] <- mean(differences)
			}
		    }
		    fern$importance <- attrImportance
		}
		return(list(fern=fern, oobPred=predMatrix))
	}

	ferns <-function(X,Y,N,D){ 
		fern<-function(i){
		#	message(sprintf("Tworzona paproć nr %s.", i))
			makeFern(X,Y,D)
		}
 		pre <- lapply( 1:N, fern )
		return(pre)
	}


	f <- ferns(X,Y,N,D)
	fernForest <- lapply(f, function(y){ y$fern })
	

  
	#obliczenie OOB
	predList <- lapply(f, function(y){ y$oobPred })
	C <- nlevels(Y)
	classMatrices <- lapply(1:C, function(classN){
		sapply(predList, function(sp){sp[,classN]})
	})

 	predMatrix <- sapply(classMatrices, function(x){rowSums(x, na.rm=TRUE)}) #sumujemy score'y
 	pred <- apply(predMatrix, 1, which.max) #i wybieramu kolumnę (klasę) o największym score

 	oobError <- function(pred,Y){ 
		pred <- as.integer(pred)
		Y <- as.integer(Y)
		return(sum(pred!=Y)/length(Y))
	}
	
	#zliczanie importance
	if(importance == TRUE){
        	imp<-lapply(fernForest, function(x) {return(x$importance)})
		#mam liste zlozona z importance'ow
	   mimp<-matrix(unlist(imp),ncol=N)
	   importance<-apply(mimp,1, function(x){mean(x, na.rm=TRUE)})
	   names(importance) <- colnames(X)
    	
		return(list(fernForest=fernForest, oobError=oobError(pred,Y), importance=importance))
        }
        else{
	 	return(list(fernForest=fernForest, oobError=oobError(pred,Y)))
    	}
}

###########################################

predictFernForestRaw<-function(fernForest,X){
	lista<-list()
	N<-length(fernForest)
	simple_prediction<-function(fern){
		predictFernRaw(fern,X)
 	}
 	lista<-lapply(fernForest, simple_prediction)
	return(lista)
}


###########################################

predictFernForest<-function(fernForest,X){

	lista<-predictFernForestRaw(fernForest,X)
	C<-ncol(lista[[1]])
	classMatrices <- lapply(1:C, function(classN) sapply(lista, function(sp){sp[,classN]}))
	predMatrix <- sapply(classMatrices, function(x){rowSums(x,na.rm=TRUE)})
	pred <- apply(predMatrix, 1, which.max)

	pred <- factor( colnames( lista[[1]])[pred], levels=colnames( lista[[1]]) )
	return(pred)
}
